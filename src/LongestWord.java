import java.util.Scanner;
import java.io.*;

public class LongestWord {
    public static void main(String [ ] args) throws FileNotFoundException {
        new LongestWord().getLongestWords();
    }

    public String getLongestWords() throws FileNotFoundException {

        String longestWord = "";
        String current;
        Scanner scan = new Scanner(new File("raamat.txt"));



        /* Kõige pikem sõna on "supernaturalists" ning selle sõna pikkus on */
        while (scan.hasNext()) {
            current = scan.useDelimiter("[^A-Za-z]+").next();
            if ((current.length() > longestWord.length())) {
                longestWord = current;
            }
        }
        System.out.println(longestWord + ": " + longestWord.length() + " tähemärki.");
        return longestWord;
    }
}