import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class AllWordUpTo8 {

    public static void main(String[] args) throws FileNotFoundException {
        new AllWordUpTo8().getLongestWords();
    }

    public String getLongestWords() throws FileNotFoundException {

        String longestWord = "";
        String current;
        Scanner scan = new Scanner(new File("raamat.txt"));

        List<String> wordList = new ArrayList<>();
        System.out.println("*** Words which frequency is greater than 50 ***");

        while (scan.hasNext()) {
            current = scan.useDelimiter("[^A-Za-z]+").next();
            if ((current.length() >= 8)) {
                longestWord = current.toLowerCase();

                wordList.add(longestWord);
                }
            }
        Set<String> unique = new HashSet<>(wordList);

        for (String word : unique) {
            if (Collections.frequency(wordList, word) > 50){
                System.out.println(word + ": " + Collections.frequency(wordList, word));
            }
        }
        /* Vastus: Kõige enim on raamatu files sõna "baskerville", mida kordub 114 korda. */
        /* Tulemus
        gutenberg: 93
        baskerville: 114
        barrymore: 73
        mortimer: 90
        stapleton: 93 */

            return longestWord;
        }
    }


